parameters:
  TIMEOUT: 10

services:
  api_client:
    class: Docupike\Api\Api
    arguments:
      - '@topic.category'
      - '@topic.class'
      - '@topic.collection'
      - '@topic.dialog'
      - '@topic.dialogEntry'
      - '@topic.entry'
      - '@topic.object'
      - '@topic.property'
      - '@topic.user'
      - '@topic.role'
      - '@topic.user-group'
      - '@topic.right'
      - '@topic.permission'
      - '@topic.token'
      - '@topic.report'
      - '@topic.setting'
      - '@topic.favorite'
      - '@topic.filter'
      - '@topic.license'
    public: true

  serializer:
    class: Symfony\Component\Serializer\Serializer
    arguments:
      - [ '@Symfony\Component\Serializer\Normalizer\ObjectNormalizer' ]
      - [ '@Symfony\Component\Serializer\Encoder\JsonEncoder' ]

  Symfony\Component\Serializer\Normalizer\ObjectNormalizer:
    arguments: [ ~, ~, ~, ~, '@Docupike\Api\Criteria\Discriminator\Resolver' ]

  Docupike\Api\Criteria\Discriminator\Resolver:
    arguments:
      - 'type'
      - or: Docupike\Api\Criteria\OrCriteria
        and: Docupike\Api\Criteria\AndCriteria
        not: Docupike\Api\Criteria\NotCriteria
        neq: Docupike\Api\Criteria\NotEqualCriteria
        type: Docupike\Api\Criteria\TypeCriteria
        lt: Docupike\Api\Criteria\LessCriteria
        lte: Docupike\Api\Criteria\LessOrEqualCriteria
        gt: Docupike\Api\Criteria\MoreCriteria
        gte: Docupike\Api\Criteria\MoreOrEqualCriteria
        not_like: Docupike\Api\Criteria\NotLikeCriteria
        like: Docupike\Api\Criteria\LikeCriteria
        eq: Docupike\Api\Criteria\EqualCriteria
        range: Docupike\Api\Criteria\RangeCriteria
        not_in: Docupike\Api\Criteria\NotInCriteria
        in: Docupike\Api\Criteria\InCriteria
        is_not_null: Docupike\Api\Criteria\IsNotNullCriteria
        is_null: Docupike\Api\Criteria\IsNullCriteria
        bit_and: Docupike\Api\Criteria\BitAndCriteria
        query: Docupike\Api\Criteria\QueryCriteria

  Symfony\Component\Serializer\Encoder\JsonEncoder: ~

  guzzle:
    class: GuzzleHttp\Client
    arguments:
      - base_uri: '%URL%'
        timeout: '%TIMEOUT%'
        handler: '%STACK%'
        headers:
          'X-API-TOKEN': '%API_KEY%'
          'content-type': 'application/json'
          connection: 'close'

  transport:
    class: Docupike\Api\Transport
    arguments: [ '@guzzle', '@serializer' ]

  topic.category:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'category'
      - '/api/v2/category'
      - '@transport'

  topic.class:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'class'
      - '/api/v2/class'
      - '@transport'

  topic.collection:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'collection'
      - '/api/v2/collection'
      - '@transport'

  topic.dialog:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'dialog'
      - '/api/v2/dialog'
      - '@transport'

  topic.dialogEntry:
    class: Docupike\Api\Topic\DialogEntry
    factory: 'Docupike\Api\Topic\DialogEntryFactory::create'
    arguments:
      - 'dialogEntry'
      - '/api/v2/dialog/{dialog-type}/entry'
      - '@transport'

  topic.entry:
    class: Docupike\Api\Topic\Entry
    factory: 'Docupike\Api\Topic\EntryFactory::create'
    arguments:
      - 'entry'
      - '/api/v2/entry/{category}'
      - '@transport'

  topic.object:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'object'
      - '/api/v2/object'
      - '@transport'

  topic.property:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'property'
      - '/api/v2/property'
      - '@transport'

  topic.user:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'user'
      - '/api/v2/user'
      - '@transport'

  topic.user-group:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'user-group'
      - '/api/v2/user-group'
      - '@transport'

  topic.role:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'role'
      - '/api/v2/role'
      - '@transport'

  topic.right:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'right'
      - '/api/v2/right'
      - '@transport'

  topic.permission:
    class: Docupike\Api\Topic\PermissionCrud
    factory: 'Docupike\Api\Topic\PermissionFactory::create'
    arguments:
      - 'permission'
      - '/api/v2/permission'
      - '@transport'

  topic.token:
    class: Docupike\Api\Topic\TokenCrud
    factory: 'Docupike\Api\Topic\TokenCrudFactory::create'
    arguments:
      - 'token'
      - '/api/v2/token'
      - '@transport'

  topic.report:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'report'
      - '/api/v2/report'
      - '@transport'

  topic.setting:
    class: Docupike\Api\Topic\SettingCrud
    factory: 'Docupike\Api\Topic\SettingFactory::create'
    arguments:
      - 'setting'
      - '/api/v2/setting'
      - '@transport'

  topic.favorite:
    class: Docupike\Api\Topic\FavoriteCrud
    factory: 'Docupike\Api\Topic\FavoriteFactory::create'
    arguments:
      - 'favorite'
      - '/api/v2/favorite'
      - '@transport'

  topic.filter:
    class: Docupike\Api\Topic\Crud
    factory: 'Docupike\Api\Topic\CrudFactory::create'
    arguments:
      - 'filter'
      - '/api/v2/filter'
      - '@transport'

  topic.license:
    class: Docupike\Api\Topic\LicenseCrud
    factory: 'Docupike\Api\Topic\LicenseFactory::create'
    arguments:
      - 'license'
      - '/api/v2/license'
      - '@transport'
