<?php declare(strict_types=1);

namespace Docupike\Api;

use Docupike\Api\Exception\Exception;
use JsonException;
use Psr\Http\Message\ResponseInterface;

class Response
{
    /**
     * @var ?ResponseInterface
     */
    private ?ResponseInterface $response = null;

    /**
     * @var string
     */
    private string $body;

    /**
     * Response constructor.
     *
     * @param ?ResponseInterface $response
     */
    public function __construct(?ResponseInterface $response)
    {
        $this->response = $response;
        $this->body = $response !== null ? $response->getBody()->getContents() : '';
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return ($this->response !== null) ? $this->response->getStatusCode() : 0;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getContentAsArray(): array
    {
        $response = $this->decodeResponse();

        if (!is_array($response)) {
            throw Exception::invalidResponseExpectedArray();
        }

        return $response;
    }

    /**
     * @return mixed
     *
     * @throws Exception
     */
    private function decodeResponse()
    {
        try {
            return json_decode($this->body, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw Exception::cannotParseResponse($exception, $this->body);
        }
    }

    /**
     * @return string
     */
    public function getRawBody(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        $response = $this->decodeResponse();

        if (!is_string($response)) {
            throw Exception::invalidResponseExpectedString();
        }

        return $response;
    }

    /**
     * @return ?ResponseInterface
     */
    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }
}
