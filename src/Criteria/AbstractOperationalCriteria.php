<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

abstract class AbstractOperationalCriteria extends AbstractCriteria
{
    /**
     * @return string
     */
    public abstract function getField(): string;
}
