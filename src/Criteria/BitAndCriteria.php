<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class BitAndCriteria extends AbstractComparableCriteria
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var int
     */
    private int $value;

    /**
     * BitAndCriteria constructor.
     *
     * @param string $field
     * @param int $value
     */
    public function __construct(string $field, int $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
