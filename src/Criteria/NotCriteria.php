<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class NotCriteria extends AbstractCriteria
{
    /**
     * @var AbstractCriteria|null
     */
    private ?AbstractCriteria $item = null;

    /**
     * NotCriteria constructor.
     *
     * @param AbstractCriteria $item
     */
    public function __construct(AbstractCriteria $item)
    {
        $this->item = $item;
    }

    /**
     * @return AbstractCriteria|null
     */
    public function getItem(): ?AbstractCriteria
    {
        return $this->item;
    }
}
