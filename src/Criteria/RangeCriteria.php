<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class RangeCriteria extends AbstractCriteria
{
    /**
     * @var bool
     */
    public const META = false;

    /**
     * @var bool
     */
    protected bool $meta = false;

    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $start;

    /**
     * @var string
     */
    private string $end;

    /**
     * @var array|null
     */
    private ?array $extra = null;

    /**
     * RangeCriteria constructor.
     *
     * @param string $field
     * @param string $start
     * @param string $end
     * @param bool $meta
     * @param array|null $extra
     */
    public function __construct(string $field, string $start, string $end, bool $meta = self::META, ?array $extra = null)
    {
        $this->field = $field;
        $this->start = $start;
        $this->end = $end;
        $this->meta = $meta;
        $this->extra = $extra;
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return array|null
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }
}
