<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

abstract class AbstractArrayCriteria extends AbstractCriteria
{
    /**
     * @var AbstractCriteria[]
     */
    private array $items = [];

    /**
     * ArrayCriteria constructor.
     *
     * @param AbstractCriteria[] $items
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    /**
     * @param AbstractCriteria $criteria
     *
     * @return void
     */
    public function add(AbstractCriteria $criteria): void
    {
        $this->items[] = $criteria;
    }

    /**
     * @return AbstractCriteria[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
