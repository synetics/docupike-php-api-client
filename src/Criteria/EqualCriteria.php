<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class EqualCriteria extends AbstractComparableCriteria
{
    /**
     * @var bool
     */
    public const META = false;

    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $value;

    /**
     * @var array|null
     */
    private ?array $extra = null;

    /**
     * EqCriteria constructor.
     *
     * @param string $field
     * @param string $value
     * @param bool $meta
     * @param array|null $extra
     */
    public function __construct(string $field, string $value, bool $meta = self::META, ?array $extra = null)
    {
        $this->field = $field;
        $this->value = $value;
        $this->meta = $meta;
        $this->extra = $extra;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return array|null
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }
}
