<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class IsNullCriteria extends AbstractOperationalCriteria
{
    /**
     * @var string
     */
    private string $field;

    /**
     * EqCriteria constructor.
     *
     * @param string $field
     */
    public function __construct(string $field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }
}
