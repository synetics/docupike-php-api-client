<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class InCriteria extends AbstractComparableCriteria
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var array
     */
    private array $value;

    /**
     * EqCriteria constructor.
     *
     * @param string $field
     * @param array $value
     */
    public function __construct(string $field, array $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->value;
    }
}
