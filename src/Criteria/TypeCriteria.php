<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class TypeCriteria extends AbstractCriteria
{
    /**
     * @var string
     */
    private string $value;

    /**
     * TypeCriteria constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
