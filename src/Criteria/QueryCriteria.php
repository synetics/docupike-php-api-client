<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class QueryCriteria extends AbstractOperationalCriteria
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var AbstractCriteria|null
     */
    private ?AbstractCriteria $criteria = null;

    /**
     * QueryCriteria constructor.
     *
     * @param string $field
     * @param AbstractCriteria|null $criteria
     */
    public function __construct(string $field, ?AbstractCriteria $criteria)
    {
        $this->field = $field;
        $this->criteria = $criteria;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return AbstractCriteria|null
     */
    public function getCriteria(): ?AbstractCriteria
    {
        return $this->criteria;
    }
}
