<?php

namespace Docupike\Api\Criteria\Discriminator;

use Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;

class Resolver implements ClassDiscriminatorResolverInterface
{
    /**
     * @var ClassDiscriminatorMapping
     */
    private ClassDiscriminatorMapping $mapping;

    /**
     * Resolver constructor.
     */
    public function __construct(string $property, array $mapping)
    {
        $this->mapping = new ClassDiscriminatorMapping($property, $mapping);
    }

    /**
     * @param string $class
     * @return ClassDiscriminatorMapping|null
     */
    public function getMappingForClass(string $class): ?ClassDiscriminatorMapping
    {
        if ($this->mapping->getMappedObjectType($class) !== null) {
            return $this->mapping;
        }

        return null;
    }

    /**
     * @param object|string $object
     * @return string|null
     */
    public function getTypeForMappedObject($object): ?string
    {
        if (null === $mapping = $this->getMappingForMappedObject($object)) {
            return null;
        }

        return $mapping->getMappedObjectType($object);
    }

    /**
     * @param object|string $object
     * @return ClassDiscriminatorMapping|null
     */
    public function getMappingForMappedObject($object): ?ClassDiscriminatorMapping
    {
        if ($this->mapping->getMappedObjectType($object) !== null) {
            return $this->mapping;
        }

        return null;
    }
}
