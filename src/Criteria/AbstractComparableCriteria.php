<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

abstract class AbstractComparableCriteria extends AbstractCriteria
{
    /**
     * @var bool
     */
    protected bool $meta = false;

    /**
     * @return string
     */
    public abstract function getField(): string;

    /**
     * @return mixed
     */
    public abstract function getValue();

    /**
     * @return bool
     */
    public function isMeta(): bool
    {
        return $this->meta;
    }
}
