<?php declare(strict_types=1);

namespace Docupike\Api\Criteria;

class IsNotNullCriteria extends IsNullCriteria
{
}
