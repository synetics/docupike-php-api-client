<?php declare(strict_types=1);

namespace Docupike\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Docupike\Api\Criteria\AbstractCriteria;
use Docupike\Api\Criteria\CriteriaInterface;
use Docupike\Api\Exception\Exception;
use Docupike\Api\Exception\ResponseException;
use Symfony\Component\Serializer\Serializer;
use Throwable;

class Transport
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    /**
     * Transport constructor.
     *
     * @param Client $client
     * @param Serializer $serializer
     */
    public function __construct(Client $client, Serializer $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @param array $body
     *
     * @return Response
     *
     * @throws Exception
     */
    public function request(string $method, string $url, array $params = [], array $body = []): Response
    {
        $parameters = [];

        if (!empty($body)) {
            $parameters[RequestOptions::JSON] = $body;
        }

        try {
            $parts = [];
            foreach ($params as $key => $value) {
                $template = '{' . $key . '}';

                if ($value instanceof AbstractCriteria) {
                    $value = $this->serializer->serialize($value, 'json');
                }

                if (strpos($url, $template) !== false) {
                    $url = str_replace($template, $value, $url);
                } else {
                    $parts[] = $key . '=' . $value;
                }
            }

            if (!empty($parts)) {
                if (strpos($url, '?') === false) {
                    $url .= '?';
                }

                $url .= implode('&', $parts);
            }

            return new Response($this->client->request($method, $url, $parameters));
        } catch (ConnectException $e) {
            throw new Exception('Networking error: ' . $e->getMessage(), (int)$e->getCode(), $e);
        } catch (RequestException $e) {
            try {
                throw new ResponseException(new Response($e->getResponse()));
            } catch (\Exception $e) {
                throw $e;
            }
        } catch (Throwable $e) {
            throw new Exception('An error occured: ' . $e->getMessage(), (int)$e->getCode(), $e);
        }
    }
}
