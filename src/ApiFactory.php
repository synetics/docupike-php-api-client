<?php declare(strict_types=1);

namespace Docupike\Api;

use GuzzleHttp\HandlerStack;
use Docupike\Api\Exception\Exception;
use Spatie\GuzzleRateLimiterMiddleware\RateLimiterMiddleware;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;
use Symfony\Component\DependencyInjection\Loader\GlobFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ApiFactory
{
    /**
     * ApiFactory constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param string $url
     * @param string $apiKey
     *
     * @return Api
     *
     * @throws Exception
     */
    public static function create(string $url, string $apiKey, ?array $rateLimiting = null): Api
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setParameter('URL', $url);
        $containerBuilder->setParameter('API_KEY', $apiKey);
        $containerBuilder->setParameter('STACK', null);
        if ($rateLimiting) {
            $stack = HandlerStack::create();

            if (isset($rateLimiting['seconds']) && is_numeric($rateLimiting['seconds'])) {
                $stack->push(RateLimiterMiddleware::perSecond($rateLimiting['seconds']));
            }

            if (isset($rateLimiting['minutes']) && is_numeric($rateLimiting['minutes'])) {
                $stack->push(RateLimiterMiddleware::perMinute($rateLimiting['minutes']));
            }

            $containerBuilder->setParameter('STACK', $stack);
        }

        $locator = new FileLocator(__DIR__ . '/..');
        $resolver = new LoaderResolver([
            new YamlFileLoader($containerBuilder, $locator),
            new GlobFileLoader($containerBuilder, $locator),
            new DirectoryLoader($containerBuilder, $locator),
        ]);

        $loader = new DelegatingLoader($resolver);
        $loader->load('config', 'directory');

        $containerBuilder->compile();
        $client = $containerBuilder->get('api_client');

        if (!$client instanceof Api) {
            throw Exception::cannotCreateClient();
        }

        return $client;
    }
}
