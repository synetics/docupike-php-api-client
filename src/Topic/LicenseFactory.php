<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Transport;

class LicenseFactory
{
    /**
     * @param string $name
     * @param string $url
     * @param Transport $transport
     *
     * @return LicenseCrud
     */
    public static function create(string $name, string $url, Transport $transport): LicenseCrud
    {
        return new LicenseCrud($name,
            new Get('Get ' . $name, $transport, $url . '/{id}'),
            new Get('Get usage ' . $name, $transport, $url . '/usage'),
            new Get('Find ' . $name, $transport, $url),
            new Post('Set ' . $name, $transport, $url),
        );
    }
}
