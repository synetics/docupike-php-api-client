<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Put;
use Docupike\Api\Transport;

class SettingFactory
{
    /**
     * @param string $name
     * @param string $url
     * @param Transport $transport
     *
     * @return SettingCrud
     */
    public static function create(string $name, string $url, Transport $transport): SettingCrud
    {
        return new SettingCrud($name,
            new Get('Get ' . $name, $transport, $url . '/{id}'),
            new Get('Get Schema ' . $name, $transport, $url . '/schema'),
            new Get('Find ' . $name, $transport, $url),
            new Put('Update system setting ' . $name, $transport, $url . '/system/{id}'),
            new Delete('Delete system ' . $name, $transport, $url . '/system/{id}'),
            new Put('Update user setting ' . $name, $transport, $url . '/user/{id}'),
            new Delete('Reset user setting ' . $name, $transport, $url . '/user/{id}')
        );
    }
}
