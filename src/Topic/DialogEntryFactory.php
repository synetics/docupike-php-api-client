<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Method\Put;
use Docupike\Api\Transport;

class DialogEntryFactory
{
    /**
     * @param string $name
     * @param string $url
     * @param Transport $transport
     *
     * @return DialogEntry
     */
    public static function create(string $name, string $url, Transport $transport): DialogEntry
    {
        return new DialogEntry($name,
            new Get('Get ' . $name, $transport, $url . '/{id}'),
            new Get('Find ' . $name, $transport, $url),
            new Post('Create ' . $name, $transport, $url),
            new Put('Update ' . $name, $transport, $url . '/{id}'),
            new Delete('Delete ' . $name, $transport, $url . '/{id}')
        );
    }
}
