<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Method\Put;

class Entry extends Topic
{
    /**
     * @var Get
     */
    private Get $get;

    /**
     * @var Get
     */
    private Get $find;

    /**
     * @var Post
     */
    private Post $create;

    /**
     * @var Put
     */
    private Put $update;

    /**
     * @var Delete
     */
    private Delete $delete;

    /**
     * Crud constructor.
     *
     * @param string $name
     * @param Get $get
     * @param Get $find
     * @param Post $create
     * @param Put $update
     * @param Delete $delete
     */
    public function __construct(string $name, Get $get, Get $find, Post $create, Put $update, Delete $delete)
    {
        parent::__construct($name, [
            $get,
            $find,
            $create,
            $update,
            $delete,
        ]);

        $this->get = $get;
        $this->find = $find;
        $this->create = $create;
        $this->update = $update;
        $this->delete = $delete;
    }

    /**
     * @param string $category
     * @param string $id
     *
     * @return array
     *
     * @throws Exception
     */
    public function get(string $category, string $id): array
    {
        return $this->get->get(['category' => $category, 'id' => $id])->getContentAsArray();
    }

    /**
     * @param string $category
     * @param int|null $offset
     * @param int|null $limit
     * @param array $parameters
     *
     * @return array
     *
     * @throws Exception
     */
    public function find(string $category, ?int $offset = null, ?int $limit = null, array $parameters = []): array
    {
        $params = array_merge($parameters);

        $params['category'] = $category;

        if ($offset !== null) {
            $params['offset'] = $offset;
        }

        if ($limit !== null) {
            $params['limit'] = $limit;
        }

        return $this->find->get($params)->getContentAsArray();
    }

    /**
     * @param string $category
     * @param array $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function create(string $category, array $item): string
    {
        return $this->create->post(['category' => $category], $item)->getContent();
    }

    /**
     * @param string $category
     * @param string $id
     * @param array $changes
     * @return string
     *
     * @throws Exception
     */
    public function update(string $category, string $id, array $changes): string
    {
        return $this->update->put(['category' => $category, 'id' => $id], $changes)->getContent();
    }

    /**
     * @param string $category
     * @param string $id
     *
     * @return string
     *
     * @throws Exception
     */
    public function delete(string $category, string $id): string
    {
        return $this->delete->delete(['category' => $category, 'id' => $id])->getContent();
    }
}
