<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Method;

abstract class Topic
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var Method[]
     */
    private array $methods = [];

    public function __construct(string $name, array $methods)
    {
        $this->name = $name;

        foreach ($methods as $method) {
            $this->addMethod($method);
        }
    }

    /**
     * @param Method $method
     *
     * @return void
     */
    private function addMethod(Method $method): void
    {
        $this->methods[$method->getName()] = $method;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Method[]
     */
    public function getMethods(): array
    {
        return $this->methods;
    }
}
