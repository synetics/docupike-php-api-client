<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Method\Put;

class FavoriteCrud extends Topic
{
    /**
     * @var Get
     */
    private Get $get;

    /**
     * @var Post
     */
    private Post $create;

    /**
     * @var Post
     */
    private Post $createMultiple;

    /**
     * @var Put
     */
    private Put $update;

    /**
     * @var Delete
     */
    private Delete $purge;

    /**
     * @var Delete
     */
    private Delete $delete;

    /**
     * Favorite constructor.
     *
     * @param string $name
     * @param Get $get
     * @param Post $create
     * @param Post $createMultiple
     * @param Put $update
     * @param Delete $purge
     * @param Delete $delete
     */
    public function __construct(string $name, Get $get, Post $create, Post $createMultiple, Put $update, Delete $purge, Delete $delete)
    {
        parent::__construct($name, [
            $get,
            $create,
            $createMultiple,
            $update,
            $purge,
            $delete,
        ]);

        $this->get = $get;
        $this->create = $create;
        $this->createMultiple = $createMultiple;
        $this->update = $update;
        $this->purge = $purge;
        $this->delete = $delete;
    }

    /**
     * @param string $type
     *
     * @return array
     *
     * @throws Exception
     */
    public function get(string $type): array
    {
        return $this->get->get(['type' => $type])->getContentAsArray();
    }

    /**
     * @param string $type
     * @param string $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function create(string $type, string $item): string
    {
        return $this->create->post(['type' => $type, 'item' => $item], [])->getContent();
    }

    /**
     * @param string $type
     * @param array $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function createMultiple(string $type, array $item): string
    {
        return $this->createMultiple->post(['type' => $type], $item)->getContent();
    }

    /**
     * @param string $type
     * @param array $changes
     * @return string
     *
     * @throws Exception
     */
    public function update(string $type, array $changes): string
    {
        return $this->update->put(['type' => $type], $changes)->getContent();
    }

    /**
     * @param string $type
     *
     * @return string
     *
     * @throws Exception
     */
    public function deleteAll(string $type): string
    {
        return $this->purge->delete(['type' => $type])->getContent();
    }

    /**
     * @param string $type
     * @param string $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function delete(string $type, string $item): string
    {
        return $this->delete->delete(['type' => $type, 'item' => $item])->getContent();
    }
}
