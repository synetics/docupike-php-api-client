<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Put;

class SettingCrud extends Topic
{
    /**
     * @var Get
     */
    private Get $get;

    /**
     * @var Get
     */
    private Get $schema;

    /**
     * @var Get
     */
    private Get $find;

    /**
     * @var Put
     */
    private Put $system;

    /**
     * @var Delete
     */
    private Delete $systemSetting;

    /**
     * @var Put
     */
    private Put $user;

    /**
     * @var Delete
     */
    private Delete $userSetting;


    /**
     * SettingCrud constructor.
     *
     * @param string $name
     * @param Get $get
     * @param Get $schema
     * @param Get $find
     * @param Put $system
     * @param Delete $systemSetting
     * @param Put $user
     * @param Delete $userSetting
     */
    public function __construct(string $name, Get $get, Get $schema, Get $find,  Put $system, Delete $systemSetting,  Put $user, Delete $userSetting)
    {
        parent::__construct($name, [
            $get,
            $schema,
            $find,
            $system,
            $systemSetting,
            $user,
            $userSetting
        ]);

        $this->get = $get;
        $this->schema = $schema;
        $this->find = $find;
        $this->system = $system;
        $this->systemSetting = $systemSetting;
        $this->user = $user;
        $this->userSetting = $userSetting;
    }

    /**
     * @param string $id
     *
     * @return string
     *
     * @throws Exception
     */
    public function get(string $id): string
    {
        return $this->get->get(['id' => $id])->getRawBody();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getSchema(): array
    {
        return $this->schema->get()->getContentAsArray();
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param array $parameters
     *
     * @return array
     *
     * @throws Exception
     */
    public function find(?int $offset = null, ?int $limit = null, array $parameters = []): array
    {
        $params = array_merge($parameters);

        if ($offset !== null) {
            $params['offset'] = $offset;
        }

        if ($limit !== null) {
            $params['limit'] = $limit;
        }

        return $this->find->get($params)->getContentAsArray();
    }

    /**
     * @param string $id
     * @param array $changes
     * @return string
     *
     * @throws Exception
     */
    public function updateSystemSetting(string $id, array $changes): string
    {
        return $this->system->put(['id' => $id], $changes)->getContent();
    }

    /**
     * @param string $id
     *
     * @return string
     *
     * @throws Exception
     */
    public function resetSystemSetting(string $id): string
    {
        return $this->systemSetting->delete(['id' => $id])->getContent();
    }

    /**
     * @param string $id
     * @param array $changes
     * @return string
     *
     * @throws Exception
     */
    public function updateUserSetting(string $id, array $changes): string
    {
        return $this->user->put(['id' => $id], $changes)->getContent();
    }

    /**
     * @param string $id
     *
     * @return string
     *
     * @throws Exception
     */
    public function resetUserSetting(string $id): string
    {
        return $this->userSetting->delete(['id' => $id])->getContent();
    }
}
