<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;

class PermissionCrud extends Topic
{
    /**
     * @var Get
     */
    private Get $get;

    /**
     * @var Get
     */
    private Get $acl;

    /**
     * @var Get
     */
    private Get $schema;

    /**
     * @var Get
     */
    private Get $scopeOption;

    /**
     * @var Get
     */
    private Get $find;

    /**
     * @var Post
     */
    private Post $create;

    /**
     * @var Delete
     */
    private Delete $delete;

    /**
     * PermissionCrud constructor.
     *
     * @param string $name
     * @param Get $get
     * @param Get $acl
     * @param Get $schema
     * @param Get $scopeOption
     * @param Get $find
     * @param Post $create
     * @param Delete $delete
     */
    public function __construct(string $name, Get $get, Get $acl, Get $schema, Get $scopeOption, Get $find, Post $create, Delete $delete)
    {
        parent::__construct($name, [
            $get,
            $acl,
            $schema,
            $scopeOption,
            $find,
            $create,
            $delete,
        ]);

        $this->get = $get;
        $this->acl = $acl;
        $this->schema = $schema;
        $this->scopeOption = $scopeOption;
        $this->find = $find;
        $this->create = $create;
        $this->delete = $delete;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function get(): array
    {
        return $this->get->get()->getContentAsArray();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getAclList(): array
    {
        return $this->acl->get()->getContentAsArray();
    }

    /**
     * @param string $parameter
     *
     * @return array
     *
     * @throws Exception
     */
    public function getScopeOption(string $parameter): array
    {
        return $this->scopeOption->get(['parameter' => $parameter])->getContentAsArray();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getSchema(): array
    {
        return $this->schema->get()->getContentAsArray();
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param array $parameters
     *
     * @return array
     *
     * @throws Exception
     */
    public function find(?int $offset = null, ?int $limit = null, array $parameters = []): array
    {
        $params = array_merge($parameters);

        if ($offset !== null) {
            $params['offset'] = $offset;
        }

        if ($limit !== null) {
            $params['limit'] = $limit;
        }

        return $this->find->get($params)->getContentAsArray();
    }

    /**
     * @param array $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function create(array $item): string
    {
        return $this->create->post([], $item)->getContent();
    }

    /**
     * @param string $id
     *
     * @return string
     *
     * @throws Exception
     */
    public function delete(string $id): string
    {
        return $this->delete->delete(['id' => $id])->getContent();
    }
}
