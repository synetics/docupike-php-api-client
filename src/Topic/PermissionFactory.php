<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Transport;

class PermissionFactory
{
    /**
     * @param string $name
     * @param string $url
     * @param Transport $transport
     *
     * @return PermissionCrud
     */
    public static function create(string $name, string $url, Transport $transport): PermissionCrud
    {
        return new PermissionCrud($name,
            new Get('Get ' . $name, $transport, $url),
            new Get('Get AclList' . $name, $transport, $url . '/acl'),
            new Get('Get Schema' . $name, $transport, $url . '/schema'),
            new Get('Get ScopeOption' . $name, $transport, $url . '/scopeOption/{parameter}'),
            new Get('Find ' . $name, $transport, $url),
            new Post('Create ' . $name, $transport, $url),
            new Delete('Delete ' . $name, $transport, $url . '/{id}')
        );
    }
}
