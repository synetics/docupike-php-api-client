<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Method\Delete;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;
use Docupike\Api\Method\Put;
use Docupike\Api\Transport;

class FavoriteFactory
{
    /**
     * @param string $name
     * @param string $url
     * @param Transport $transport
     *
     * @return FavoriteCrud
     */
    public static function create(string $name, string $url, Transport $transport): FavoriteCrud
    {
        return new FavoriteCrud($name,
            new Get('Get type' . $name, $transport, $url . '/{type}'),
            new Post('Create ' . $name, $transport, $url . '/{type}/{item}'),
            new Post('Create multiple ' . $name, $transport, $url . '/{type}'),
            new Put('Update ' . $name, $transport, $url . '/{type}'),
            new Delete('Remove ' . $name, $transport, $url . '/{type}'),
            new Delete('Purge ' . $name, $transport, $url . '/{type}/{item}'),
        );
    }
}
