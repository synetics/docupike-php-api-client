<?php declare(strict_types=1);

namespace Docupike\Api\Topic;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Method\Get;
use Docupike\Api\Method\Post;

class LicenseCrud extends Topic
{
    /**
     * @var Get
     */
    private Get $get;

    /**
     * @var Get
     */
    private Get $usage;

    /**
     * @var Get
     */
    private Get $find;

    /**
     * @var Post
     */
    private Post $set;

    /**
     * Crud constructor.
     *
     * @param string $name
     * @param Get $get
     * @param Get $usage
     * @param Get $find
     * @param Post $set
     */
    public function __construct(string $name, Get $get, Get $usage, Get $find, Post $set)
    {
        parent::__construct($name, [
            $get,
            $usage,
            $find,
            $set,
        ]);

        $this->get = $get;
        $this->usage = $usage;
        $this->find = $find;
        $this->set = $set;
    }

    /**
     * @param string $id
     *
     * @return array
     *
     * @throws Exception
     */
    public function get(string $id): array
    {
        return $this->get->get(['id' => $id])->getContentAsArray();
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function usage(): array
    {
        return $this->usage->get()->getContentAsArray();
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param array $parameters
     *
     * @return array
     *
     * @throws Exception
     */
    public function find(?int $offset = null, ?int $limit = null, array $parameters = []): array
    {
        $params = array_merge($parameters);

        if ($offset !== null) {
            $params['offset'] = $offset;
        }

        if ($limit !== null) {
            $params['limit'] = $limit;
        }

        return $this->find->get($params)->getContentAsArray();
    }

    /**
     * @param array $item
     *
     * @return string
     *
     * @throws Exception
     */
    public function set(array $item): string
    {
        return $this->set->post([], $item)->getContent();
    }
}
