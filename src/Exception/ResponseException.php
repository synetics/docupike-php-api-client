<?php declare(strict_types=1);

namespace Docupike\Api\Exception;

use Docupike\Api\Response;

class ResponseException extends Exception
{
    /**
     * @var Response
     */
    private Response $response;

    /**
     * ResponseException constructor.
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;

        try {
            $responseContent = $this->response->getContentAsArray();
        } catch (\Exception $e) {
            $responseContent = ['message' => $e->getMessage()];
        }

        try {
            parent::__construct($responseContent['message'], $response->getStatusCode());
        } catch (\Exception $e) {
            parent::__construct($responseContent['status_message'], $response->getStatusCode());
        }
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }
}
