<?php declare(strict_types=1);

namespace Docupike\Api\Exception;

use Exception as BaseException;
use JsonException;

class Exception extends BaseException
{
    /**
     * @return self
     */
    public static function cannotCreateClient(): self
    {
        return new self('Cannot create API Client');
    }

    /**
     * @param JsonException $jsonException exception thrown when response does not contain valid Json
     * @param string $responseBody body of API response
     *
     * @return self
     */
    public static function cannotParseResponse(JsonException $jsonException, string $responseBody): self
    {
        return new self('Cannot parse API Response: ' . $responseBody, 400, $jsonException);
    }

    /**
     * @return self
     */
    public static function invalidResponseExpectedArray(): self
    {
        return new self('Invalid response. Response expected to be an array');
    }

    /**
     * @return self
     */
    public static function invalidResponseExpectedString(): self
    {
        return new self('Invalid response. Response expected to be a string');
    }
}
