<?php declare(strict_types=1);

namespace Docupike\Api;

use Docupike\Api\Topic\Crud;
use Docupike\Api\Topic\DialogEntry;
use Docupike\Api\Topic\Entry;
use Docupike\Api\Topic\FavoriteCrud;
use Docupike\Api\Topic\LicenseCrud;
use Docupike\Api\Topic\PermissionCrud;
use Docupike\Api\Topic\SettingCrud;
use Docupike\Api\Topic\TokenCrud;

class Api
{
    /**
     * @var Crud
     */
    public Crud $category;

    /**
     * @var Crud
     */
    public Crud $class;

    /**
     * @var Crud
     */
    public Crud $collection;

    /**
     * @var Crud
     */
    public Crud $dialog;

    /**
     * @var DialogEntry
     */
    public DialogEntry $dialogEntry;

    /**
     * @var Entry
     */
    public Entry $entry;

    /**
     * @var Crud
     */
    public Crud $object;

    /**
     * @var Crud
     */
    public Crud $property;

    /**
     * @var Crud
     */
    public Crud $user;

    /**
     * @var Crud
     */
    public Crud $role;

    /**
     * @var Crud
     */
    public Crud $userGroup;

    /**
     * @var Crud
     */
    public Crud $right;

    /**
     * @var PermissionCrud
     */
    public PermissionCrud $permission;

    /**
     * @var TokenCrud
     */
    public TokenCrud $token;

    /**
     * @var Crud
     */
    public Crud $report;

    /**
     * @var SettingCrud
     */
    public SettingCrud $setting;

    /**
     * @var FavoriteCrud
     */
    public FavoriteCrud $favorite;

    /**
     * @var Crud
     */
    public Crud $filter;

    /**
     * @var LicenseCrud
     */
    public LicenseCrud $license;

    /**
     * Api constructor.
     *
     * @param Crud $category
     * @param Crud $class
     * @param Crud $collection
     * @param Crud $dialog
     * @param DialogEntry $dialogEntry
     * @param Entry $entry
     * @param Crud $object
     * @param Crud $property
     * @param Crud $user
     * @param Crud $role
     * @param Crud $userGroup
     * @param Crud $right
     * @param PermissionCrud $permission
     * @param TokenCrud $token
     * @param Crud $report
     * @param SettingCrud $setting
     * @param FavoriteCrud $favorite
     * @param Crud $filter
     * @param LicenseCrud $license
     */
    public function __construct(
        Crud $category,
        Crud $class,
        Crud $collection,
        Crud $dialog,
        DialogEntry $dialogEntry,
        Entry $entry,
        Crud $object,
        Crud $property,
        Crud $user,
        Crud $role,
        Crud $userGroup,
        Crud $right,
        PermissionCrud $permission,
        TokenCrud $token,
        Crud $report,
        SettingCrud $setting,
        FavoriteCrud $favorite,
        Crud $filter,
        LicenseCrud $license
    )
    {
        $this->category = $category;
        $this->class = $class;
        $this->collection = $collection;
        $this->dialog = $dialog;
        $this->dialogEntry = $dialogEntry;
        $this->entry = $entry;
        $this->object = $object;
        $this->property = $property;
        $this->user = $user;
        $this->role = $role;
        $this->userGroup = $userGroup;
        $this->right = $right;
        $this->permission = $permission;
        $this->token = $token;
        $this->report = $report;
        $this->setting = $setting;
        $this->favorite = $favorite;
        $this->filter = $filter;
        $this->license = $license;
    }
}
