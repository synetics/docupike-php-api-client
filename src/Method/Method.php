<?php declare(strict_types=1);

namespace Docupike\Api\Method;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Response;
use Docupike\Api\Transport;

abstract class Method
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var Transport
     */
    private Transport $transport;

    /**
     * @var string
     */
    private string $url;

    public function __construct(string $name, Transport $transport, string $url)
    {
        $this->name = $name;
        $this->transport = $transport;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $method
     * @param array $parameters
     * @param array $body
     *
     * @return Response
     *
     * @throws Exception
     */
    protected function process(string $method, array $parameters = [], array $body = []): Response
    {
        return $this->transport->request($method, $this->url, $parameters, $body);
    }
}
