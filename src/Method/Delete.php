<?php declare(strict_types=1);

namespace Docupike\Api\Method;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Response;

class Delete extends Method
{
    /**
     * @param array $parameters
     *
     * @return Response
     *
     * @throws Exception
     */
    public function delete(array $parameters): Response
    {
        return $this->process('DELETE', $parameters);
    }
}
