<?php declare(strict_types=1);

namespace Docupike\Api\Method;

use Docupike\Api\Exception\Exception;
use Docupike\Api\Response;

class Options extends Method
{
    /**
     * @param array $parameters
     * @param array $body
     *
     * @return Response
     *
     * @throws Exception
     */
    public function options(array $parameters, array $body): Response
    {
        return $this->process('OPTIONS', $parameters, $body);
    }
}
