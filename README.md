# docupike cloud API Client

This is the library to work with docupike cloud Application.

## How to install?

Run the following command to require library via composer

```
composer require docupike/api-client
```

## How to use?

Create the API Client with the authorisation data: url of your docupike instance and the api-token

```php
$api = ApiFactory::create(
        'http://docupike.instance/',
        'your-api-token'
    );
```

Then you can call the api methods via this object:

```php
$users = $api->user->find();
```

### Using criteria in `find` requests

If you want to find certain users, objects or entries by specific conditions you can use "Criterias" to do so.
See this example, which will find all entries of the general category with titles either like 'example' or 'server'
but not exactly 'Server-Prod' (please note the case of 'Server-Prod').

```php
use Docupike\Api\Criteria\AndCriteria;
use Docupike\Api\Criteria\LikeCriteria;
use Docupike\Api\Criteria\NotEqualCriteria;
use Docupike\Api\Criteria\OrCriteria;

$criteria = new OrCriteria([
    new LikeCriteria('cmdb.general.title', 'example'),
    new AndCriteria([
        new LikeCriteria('cmdb.general.title', 'server'),
        new NotEqualCriteria('cmdb.general.title', 'Server-Prod')
    ])
]);

$entries = $api->entry->find('cmdb.general', null, null, ['criteria' => $criteria]);
```

The criteria can be understood like this:

```
Find all entries where:

general.title is like 'example'
or (
    general.title is like 'server'
    and general.title is not exactly 'Server-Prod'
)
```

You can find all available criteria classes in `src/Criteria`.

## How it works?

API uses Guzzle to process the requests to the i-doit API.

**Api** is a facade that contains the possible API topics: object, category, class, user etc

Every API **Topic** has methods to work with the given topic: object->create, object->update, object->find etc

**Methods** have

* the endpoint to call with the HTTP method
* response type - what should be the result of the call
* response schema (to be done) - to make the validation
* request schema (to be done) - to make the validation

**Transport** proceeds the request, parses the response to the needed type, handles the errors and throws the corresponding exceptions.

